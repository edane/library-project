// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDtUw8xLGyjwZEWDIshFzJXRI1iBBrm_W0",
    authDomain: "library-project-4c1ab.firebaseapp.com",
    projectId: "library-project-4c1ab",
    storageBucket: "library-project-4c1ab.appspot.com",
    messagingSenderId: "546366691116",
    appId: "1:546366691116:web:32432372561bf5afdd7e89",
    measurementId: "G-SFWCJ8LFJ0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
