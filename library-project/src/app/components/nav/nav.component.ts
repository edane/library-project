import { Component, OnInit } from '@angular/core';
import {AuthenticateService} from "../../services/authenticate.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private authService: AuthenticateService) { }
  isAdmin = false;
  ngOnInit(): void {
    this.authService.myRole.subscribe( (value) => this.isAdmin = value === 'admin');
  }



}
