import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import { CreateBookComponent } from './create-book/create-book.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookComponent } from './book/book.component';
import {AvailabilityPipe} from "../_pipes/availability.pipe";
import {RouterModule} from "@angular/router";
import {OverviewComponent} from "./overview/overview.component";
import {BookAuthorComponent} from "./book-author/book-author.component";
import {AppBackgroundDirective} from "./directives/app-background.directive";



@NgModule({
  declarations: [
    CreateBookComponent,
    BookListComponent,
    BookComponent,
    AvailabilityPipe,
    OverviewComponent,
    BookAuthorComponent,
    AppBackgroundDirective
  ],
  exports: [
    CreateBookComponent,
    BookListComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule
  ]
})
export class BooksModule { }
