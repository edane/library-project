import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable} from "rxjs";

@Injectable({providedIn: 'root'})
export class AuthenticateService {
  loggedIn = false;
  private role: BehaviorSubject<string> = new BehaviorSubject<string>('');
  myRole: Observable<string> = this.role.asObservable();

  isLoggedIn():boolean {
    return  this.loggedIn;
  }

  setLoggedIn(loggedIn: boolean): void {
    this.loggedIn = loggedIn;
  }

  setRole(role: string) {
    this.role.next(role);
  }

}
